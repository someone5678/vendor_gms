#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifneq ($(TARGET_SHIP_GMS),false)

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    AndroidPlatformServices \
    MlkitBarcodeUIPrebuilt \
    VisionBarcodePrebuilt

# Artifact path requirement allowlist
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \
    system/etc/permissions/privapp-permissions-google.xml \
    system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk \
    system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk \
    system/priv-app/TagGoogle/TagGoogle.apk

# Recorder
TARGET_SUPPORTS_GOOGLE_RECORDER ?= true
ifeq ($(TARGET_SUPPORTS_GOOGLE_RECORDER),true)
PRODUCT_PACKAGES += \
    RecorderPrebuilt
endif

# arcore
TARGET_INCLUDE_STOCK_ARCORE ?= true
ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

# Live Wallpapers
TARGET_INCLUDE_LIVE_WALLPAPERS ?= true
ifeq ($(TARGET_INCLUDE_LIVE_WALLPAPERS),true)
PRODUCT_PACKAGES += \
    PixelLiveWallpaperPrebuilt
endif

# Quick Tap
TARGET_SUPPORTS_QUICK_TAP ?= true
ifeq ($(TARGET_SUPPORTS_QUICK_TAP),true)
PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/product/etc/sysconfig/quick_tap.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/quick_tap.xml
endif

# Call recording on Google Dialer
TARGET_SUPPORTS_CALL_RECORDING ?= true
ifeq ($(TARGET_SUPPORTS_CALL_RECORDING),true)
PRODUCT_PACKAGES += \
    com.google.android.apps.dialer.call_recording_audio.features
endif

# Charger Resources
ifneq ($(TARGET_CHARGER_RESOURCE_COPY_OUT),)
CHARGER_RESOURCE_COPY_OUT := $(TARGET_CHARGER_RESOURCE_COPY_OUT)
else
CHARGER_RESOURCE_COPY_OUT := $(TARGET_COPY_OUT_PRODUCT)
endif

PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/product/etc/res/images/charger/battery_fail.png:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/images/charger/battery_fail.png \
    vendor/gms/common/proprietary/product/etc/res/images/charger/battery_scale.png:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/images/charger/battery_scale.png \
    vendor/gms/common/proprietary/product/etc/res/images/charger/main_font.png:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/images/charger/main_font.png \
    vendor/gms/common/proprietary/product/etc/res/values/charger/animation.txt:$(CHARGER_RESOURCE_COPY_OUT)/etc/res/values/charger/animation.txt

# DMService libs symlinks
PRODUCT_PACKAGES += \
    LibDMSymLink

# Dex preopt
PRODUCT_DEXPREOPT_SPEED_APPS += \
    NexusLauncherRelease

ifneq ($(filter blueline crosshatch bonito sargo flame coral sunfish bramble redfin oriole raven bluejay panther cheetah lynx, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    TurboAdapter

PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/system_ext/lib64/libpowerstatshaldataprovider.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/libpowerstatshaldataprovider.so

PRODUCT_PACKAGES += \
    LibPowerStatsSymLink
endif

ifneq ($(filter flame coral sunfish bramble barbet redfin oriole raven panther cheetah, $(TARGET_DEVICE)),)
PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/product/etc/sysconfig/adaptivecharging.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/adaptivecharging.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml
endif

ifneq ($(filter bonito sargo flame coral sunfish bramble barbet redfin oriole raven bluejay panther cheetah, $(TARGET_DEVICE)),)
PRODUCT_PACKAGES += \
    GoogleCamera
endif

ifneq ($(filter blueline crosshatch flame coral redfin oriole raven panther cheetah, $(TARGET_DEVICE)),)
PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/product/etc/sysconfig/dreamliner.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/dreamliner.xml \
    vendor/gms/common/proprietary/product/etc/permissions/com.google.android.apps.dreamliner.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.apps.dreamliner.xml

PRODUCT_PACKAGES += \
    DreamlinerPrebuilt \
    DreamlinerUpdater
endif

ifneq ($(filter oriole raven panther cheetah lynx, $(TARGET_DEVICE)),)
PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2022.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2022.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2022_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2022_midyear.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2021.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2021.xml
endif

$(call inherit-product, vendor/gms/common/common-vendor.mk)

# Properties
$(call inherit-product, vendor/gms/products/properties.mk)

# Pixel Framework
$(call inherit-product-if-exists, vendor/pixel-framework/config.mk)

# Customizations
$(call inherit-product, vendor/gms/products/custom.mk)

endif
