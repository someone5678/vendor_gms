# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/gms/products/overlay

PRODUCT_PACKAGE_OVERLAYS += \
    vendor/gms/products/overlay/common

# GMS RRO overlay
PRODUCT_PACKAGES += \
    PixelConfigOverlayCustom \
    SettingsGoogleOverlayCustom \
    SystemUIGoogleOverlayCustom \
    DocumentsUIGoogleOverlayCustom \
    PixelLauncherOverlayCustom

# Launcher overlay
ifeq ($(TARGET_DEVICE_IS_TABLET),true)
PRODUCT_PACKAGES += \
    NexusLauncherTabletOverlay
endif

# Custom Google apps power whitelist
PRODUCT_PACKAGES += \
    custom-google-power-whitelist

# Bootanimation symlinks
PRODUCT_PACKAGES += \
    BootanimationSymLink

# Themed icons for Pixel Launcher
$(call inherit-product, packages/overlays/ThemeIcons/config.mk)
