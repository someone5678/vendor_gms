#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DEVICE=common
VENDOR=gms

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

ANDROID_ROOT="${MY_DIR}/../.."

HELPER="${ANDROID_ROOT}/tools/extract-utils/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

# Initialize the helper
setup_vendor "${DEVICE}" "${VENDOR}" "${ANDROID_ROOT}" true

# Warning headers and guards
write_headers "arm64"
sed -i 's|TARGET_DEVICE|TARGET_ARCH|g' "${ANDROIDMK}"
sed -i 's|vendor/gms/|vendor/gms/common|g' "${PRODUCTMK}"
sed -i 's|device/gms//setup-makefiles.sh|vendor/gms/setup-makefiles.sh|g' "${ANDROIDBP}" "${ANDROIDMK}" "${BOARDMK}" "${PRODUCTMK}"
cat "${MY_DIR}/gmscore-mk.txt" >> "${ANDROIDMK}"

write_makefiles "${MY_DIR}/proprietary-files.txt" true

# Finish
write_footers

# Overlays
echo -e "\ninclude vendor/gms/common/overlays.mk" >> $PRODUCTMK

# Exclusions
sed -i '/dreamliner/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/Dreamliner/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/GCS/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/SCONE/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/adaptivecharging/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/TurboAdapter/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/libpowerstatshaldataprovider/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/verizon/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/Tycho/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/VZWAPNLib/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/AppDirectedSMSService/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/GoogleCamera/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/com.google.android.camera/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/google-ril/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/PlayAutoInstallConfig/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/Videos/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/YouTube/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/RecorderPrebuilt/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/arcore/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/PixelLiveWallpaperPrebuilt/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/quick_tap/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/pixel_experience_2021.xml/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/pixel_experience_2022.xml/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/pixel_experience_2022_midyear.xml/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/pixel_experience_2023.xml/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/pixel_experience_2023_midyear.xml/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"

sed -i '/battery_fail.png/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/battery_scale.png/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/main_font.png/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
sed -i '/animation.txt/d' "${ANDROID_ROOT}/vendor/gms/common/common-vendor.mk"
